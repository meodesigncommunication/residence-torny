<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //

define( 'DB_NAME', 'tornyresidence' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'root' );
define( 'DB_HOST', 'localhost' );

/*define( 'DB_NAME', 'atzg_torny' );
define( 'DB_USER', 'atzg_torny' );
define( 'DB_PASSWORD', 'jyV3eA69EWrD' );
define( 'DB_HOST', 'atzg.myd.infomaniak.com' );*/


define( 'DB_CHARSET', 'utf8mb4' );

define( 'IS_PRODUCTION', true );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'doKB 2U>Y[:d^!2}JeWXc}U#0ap)YTrZK^.9Q*#UF$~gmuJ5p,{X,>2)n-#I3#[L' );
define( 'SECURE_AUTH_KEY',  'w&Wikdxt{tK*1AEZO0xQv}0kV=|XEVTzh{^0>*88dK-yC1_Nz8>iQdbm%;D/Z36|' );
define( 'LOGGED_IN_KEY',    'M0f3@{)a8_hd[/,CuZ?=L{{<E 4[0gQQlPI9zcVyKn~c]P>hxhD-b:|0`Lk!khu-' );
define( 'NONCE_KEY',        'Sz9;?8x{kif&6iB]R4_Uw9!|yc6Mmd#3`[K8O`ziwa0[N;3F98 Zz9^;4#<>P.iE' );
define( 'AUTH_SALT',        '12S=U#zL<^?3%7ljN=hNq3rWy0Q$*Wro#]} VD;>&@4|6Ps+ 1;*WM$lKRStYd|m' );
define( 'SECURE_AUTH_SALT', 'B=DqF]G:XeLx(3noDZ<%/T]Tu(Z]5cJtn|VDh{*XU7m[Y_Bp:O2Jpdpa=weHdV a' );
define( 'LOGGED_IN_SALT',   'R}b{N@5]nvUTJ3y$l6BEzI ?C`)Kv1aRH8g@glqhJSwGb;8_ $h[|&p|`ZUf8ED!' );
define( 'NONCE_SALT',       '>gC_q+Kh5i^~P2Rl9uRU>w4I0JvibZ*?$Cq&</fp8/^=UOTSKWup>G|>[2B{5L[+' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
