<?php

global $wpdb;

if ( ! class_exists( 'Timber' ) ) {
    echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
    return;
}

$post = new TimberPost();
$data = Timber::get_context();
$data['site_url'] = get_site_url();
$data['post'] = $post;
$data['options'] = wp_load_alloptions();
$data['analytics_id'] = $_COOKIE['analytics_id'];

$page_popup_id = get_field('page_popup_id', $post->ID);
$file_request = get_post($page_popup_id);
$data['file_request'] = $file_request->post_name;


/*
 * Navigation
 */
$menus = array();
$wp_navigations = wp_get_nav_menu_items('main');

if(isset($wp_navigations) && !empty($wp_navigations)) {
    foreach($wp_navigations as $nav)
    {
        if($nav->menu_item_parent != 0)
        {
            $menus[$nav->menu_item_parent]['childs'][$nav->ID]['title'] = $nav->title;
            $menus[$nav->menu_item_parent]['childs'][$nav->ID]['url'] = $nav->url;
            $menus[$nav->menu_item_parent]['childs'][$nav->ID]['object_id'] = $nav->object_id;
        }else {
            $menus[$nav->ID]['title'] = $nav->title;
            $menus[$nav->ID]['url'] = $nav->url;
            $menus[$nav->ID]['object_id'] = $nav->object_id;
        }
    }
}

$data['menus'] = $menus;

/*
 *  Include post Thumb
 */
$data['featured_image'] = (get_the_post_thumbnail_url( $post->ID, 'full' )) ? get_the_post_thumbnail_url( $post->ID, 'full' ) : '';

/*
 *  Include path
 */
$data['template_path'] = get_template_directory();
$data['template_path_uri'] = get_template_directory_uri();
$data['plugin_path'] = plugin_dir_path( __FILE__ . '../../plugins/' );
$data['plugin_url'] = plugins_url();

/*
 *  Include ACF field in page
 */
$data['page_items'] = get_field('page_items', $post->ID);
$data['metadata'] = get_field('metadata', $post->ID);

$data['logos_footer'] = get_field('logos_footer', 'option');
$data['header_info_status'] = get_field('enabled_info_header', 'option');

if($data['header_info_status'] == 'enabled'){
    $data['header_info']['color'] = get_field('header_info_color', 'option');
    $data['header_info']['txt'] = get_field('header_info_txt', 'option');
}

$data['lateral_popup'] = get_field('lateral_popup', $post->ID);

/*
 *  Footer show
 */
$data['show_footer_contact'] = true;

/*echo '<pre>';
print_r($data['page_items']);
echo '</pre>'; exit();*/
