<?php
/*
 * Template name: TPL Landingpage
 */

$post = new TimberPost();
$context = Timber::get_context();
$context['post'] = $post;
$context['template_path'] = get_template_directory_uri();
$context['analytics_id'] = $_COOKIE['analytics_id'];
$context['form_contact'] = do_shortcode('[contact-form-7 id="5" title="Contact form 1"]');


Timber::render( array( 'page-' . $post->post_name . '.twig', 'landingpage.twig' ), $context );