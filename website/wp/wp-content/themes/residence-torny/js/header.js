$(function(){

    scrollHeader();

    $(window).scroll(function(){
        scrollHeader();
    });

    $('nav i.icon-bars').click(function(){
        mobileMenu();
    });

    $('nav i.icon-close-btn').click(function(){
        mobileMenu();
    });
});

function mobileMenu() {

    if( $('nav i.icon').hasClass('icon-bars') ) {

        $('nav i.icon').addClass('icon-close-btn');
        $('nav i.icon').removeClass('icon-bars');
        $('ul.menu').addClass('opened');

    }else{

        $('nav i.icon').addClass('icon-bars');
        $('nav i.icon').removeClass('icon-close-btn');
        $('ul.menu').removeClass('opened');

    }

}

function scrollHeader() {
    if($(window).scrollTop() > 40) {
        $('header.header').addClass('scroll');
    }else{
        $('header.header').removeClass('scroll');
    }
}