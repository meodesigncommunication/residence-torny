<?php
/**
 * Template name: Homepage
 */

$templates = array( 'homepage.html.twig' );
require_once 'timber-includes/timber.php';
$data['class'] = 'homepage-template';
$data['slider'] = get_field('slider', $post->ID);
$data['content_slideshow'] = get_field('content_slideshow', $post->ID);

Timber::render( $templates, $data );