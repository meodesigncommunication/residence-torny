<?php
/**
 * Template name: TPL Contact
 */

$templates = array( 'page-contact.html.twig' );
require_once 'timber-includes/timber.php';
$data['class'] = 'page-template tpl-contact';

$data['show_footer_contact'] = false;
$data['form_contact'] = do_shortcode('[contact-form-7 id="5" title="Contact form 1"]');

Timber::render( $templates, $data );
